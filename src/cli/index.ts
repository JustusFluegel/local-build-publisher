import parseArgs, {ParsedArgs} from "minimist";
import * as path from "path";
import * as fs from "fs";
import { GitCommit, gitToJs } from "git-parse";
import { exec, spawnSync } from "child_process";

export const cli =  async (argv: string[]) => {
    const {_: positionalArgs, registry}: ParsedArgs = parseArgs(argv.slice(2))
    const uri: string = positionalArgs.length >= 1? path.resolve(positionalArgs[0]): process.cwd()
    let git: GitCommit[] | undefined = undefined;
    if(!fs.existsSync(uri)) throw new Error("Path not found! Did you provide an invalid path?");
    if(!fs.existsSync(path.join(uri, "package.json"))) throw new Error("Couldn't find package.json. Are you in the right folder or did you provide the correct path?");
    if(fs.existsSync(path.join(uri,".git")) && (await fs.promises.stat(path.join(uri, ".git"))).isDirectory) {
        console.info("Info: detected .git dir, activating git integration...");
        git = await gitToJs(uri)
        console.log("\n Current git status:\n")
        const isClean = await new Promise((res, rej) => exec('git status -s',{cwd: uri, windowsHide: true}, (err, stdo) => {
            console.log(stdo)
            if(err) rej(err)
            else res(stdo.length == 0)
        }))
        if(!isClean) throw new Error("Git repo found and working directory not clean - commit and try again")
    }
    const oldFile = (await fs.promises.readFile(path.join(uri, "package.json"), "utf-8")).toString();
    const jsonData = JSON.parse(oldFile)
    if(jsonData.version == undefined || typeof jsonData.version != "string") throw new Error("Something went wrong - package.json has no version?")
    jsonData.version = `${jsonData.version}-build-${(new Date(Date.now())).toISOString().split(":").join("--").split(".").join("---").split("+").join("----")}${git != undefined? `-${git[git.length -1].hash.slice(0,8)}`: ""}`
    await fs.promises.writeFile(path.join(uri, "package.json"), JSON.stringify(jsonData), "utf-8");
    spawnSync(registry == undefined || typeof registry !== "string"? "npm publish": `npm publish --registry ${registry}`, {stdio: "inherit", shell: true});
    await fs.promises.writeFile(path.join(uri, "package.json"), oldFile, "utf-8");
};